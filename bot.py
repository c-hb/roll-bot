import praw, config, db, random, time

def login():
	return praw.Reddit(username=config.username,
				password=config.password,
				client_id=config.client_id,
				client_secret=config.client_secret,
				user_agent=config.user_agent)

def run(r, sub=config.sub):
	for comment in r.subreddit(sub).comments(limit=25):
		if "rollx" in comment.body.lower() and not db.replied_cmt(comment.id):
			comment.reply("You've rolled a {}.".format(roll()))
			db.add_cmt(comment.id)
	time.sleep(10)

def roll(lim=config.lim):
	return random.randint(1, lim)

def main():
	db.create()
	r = login()
	while True:
		try:
			run(r)
		except KeyboardInterrupt:
			raise
		# except:
		# 	pass

if __name__=='__main__':
	main()







