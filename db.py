import sqlite3, config

conn = sqlite3.connect(config.db_name)
c = conn.cursor()

def create():
	c.execute('create table if not exists subms (id integer primary key autoincrement, subm_id text)')
	c.execute('create table if not exists cmts (id integer primary key autoincrement, cmt_id text)')
	save()

def clear():
	del_tables()
	create()
	save()

def del_tables():
	tables = [t[0] for t in get_tables()]
	for t in tables:
		print t
		if t != "sqlite_sequence":
			c.execute('drop table if exists {}'.format(t))
	save()

def save():
	try:
		conn.commit()
	except:
		conn.rollback()

def add_cmt(id):
	c.execute('insert into cmts(cmt_id) values(?)', (id,))
	save()


def replied_cmt(id):
	c.execute('select * from cmts where cmt_id like ?', (id,))
	return len(c.fetchall()) > 0

def table_exists(name):
	table = c.execute("select name from sqlite_master where type='table' and name=?", (name,))
	return table.fetchone() is not None


def get_cmts():
	try:
		return c.execute('select * from cmts')
	except:
		return None

def get_tables():
	try:
		return c.execute("select name from sqlite_master where type='table' order by name")
	except:
		return None

def list():
	if get_tables() is not None:
		print get_tables().fetchall()
	if get_cmts() is not None:
		print get_cmts().fetchall()